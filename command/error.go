package command

var errInvalidArgs = &ErrInvalidArgs{}

type ErrInvalidArgs struct{}

func (e ErrInvalidArgs) Error() string {
	return "Invalid arguments"
}

func (e ErrInvalidArgs) ExitCode() int {
	return 2
}
