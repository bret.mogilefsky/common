package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/template/plugin" // TODO
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "XXX analyzer for GitLab SAST" // TODO
	app.Author = "GitLab"

	app.Commands = command.NewCommands(command.Config{
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func match(path string, info os.FileInfo) (bool, error) {
	if info.Name() == "XXX" { // TODO
		return true, nil
	}
	return false, nil
}
