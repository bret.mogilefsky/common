package issue

import (
	"encoding/json"
	"fmt"
	"strconv"
)

type Issue struct {
	Tool        string       `json:"tool"`                  // Tool identifies the analyzer.
	Category    Category     `json:"category"`              // Category describes where this vulnerability belongs (SAST, Dependency Scanning, etc...)
	Name        string       `json:"name,omitempty"`        // Name of the vulnerability, this must not include occurence's specific information.
	Message     string       `json:"message,omitempty"`     // Message is a short text that describes the vulnerability, it may include occurence's specific information.
	Description string       `json:"description,omitempty"` // Description is a long text that describes the vulnerability.
	CompareKey  string       `json:"cve"`                   // CompareKey is a value used to establish whether two issues are the same.
	Severity    Level        `json:"severity,omitempty"`    // Severity describes how much the vulnerability impacts the software.
	Confidence  Level        `json:"confidence,omitempty"`  // Confidence describes how the vulnerability is likely to impact the software.
	Solution    string       `json:"solution,omitempty"`    // Solution explains how to fix the vulnerability.
	Location    Location     `json:"location,omitempty"`    // Location tells which class and/or method is affected by the vulnerability.
	Identifiers []Identifier `json:"identifiers,omitempty"` // Identifiers are references that identify a vulnerability on internal or external DBs.
	Links       []Link       `json:"links,omitempty"`       // Links are external documentations or articles that further describes the vulnerability.
}

// MarshalJSON allows to customize json output to provide backward compatible
// format while keeping current Issue struct clean.
func (i Issue) MarshalJSON() ([]byte, error) {
	type EmbeddedIssue Issue
	outputIssue := struct {
		*EmbeddedIssue
		Priority Level  `json:"priority,omitempty"` // **Deprecated: moved into Severity** Priority describes how important is the vulnerability.
		File     string `json:"file,omitempty"`     // **Deprecated: moved into Location** File is the path relative to the search path.
		Line     int    `json:"line,omitempty"`     // **Deprecated: moved into Location and renamed into LineStart** Line is the first line of the affected code.
		URL      string `json:"url,omitempty"`      // **Deprecated: moved into Links** URL is where more documentation about the vulnerability can be found.
	}{
		EmbeddedIssue: (*EmbeddedIssue)(&i),
		Priority:      i.Severity,
		File:          i.Location.File,
		Line:          i.Location.LineStart,
	}

	if len(i.Links) > 0 {
		outputIssue.URL = i.Links[0].URL
	}

	if outputIssue.URL == "" && len(i.Identifiers) > 0 {
		for _, identifier := range i.Identifiers {
			if identifier.URL != "" {
				outputIssue.URL = identifier.URL
				break
			}
		}
	}

	return json.Marshal(outputIssue)
}

type Category string

const (
	CategorySast               = "sast"
	CategoryDependencyScanning = "dependency_scanning"
	CategoryContainerScanning  = "container_scanning"
	CategoryDast               = "dast"
)

// Level type used by severity and confidence values
type Level int

const (
	LevelUndefined Level = iota
	LevelIgnore
	LevelUnknown
	LevelExperimental
	LevelLow
	LevelMedium
	LevelHigh
	LevelCritical
)

// MarshalJSON is used to convert a Level object into a JSON representation
func (l Level) MarshalJSON() ([]byte, error) {
	return json.Marshal(l.String())
}

// UnmarshalJSON is used to convert a JSON representation into a Level object
func (l *Level) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	switch s {
	case "Critical":
		*l = LevelCritical
	case "High":
		*l = LevelHigh
	case "Medium":
		*l = LevelMedium
	case "Low":
		*l = LevelLow
	case "Experimental":
		*l = LevelExperimental
	case "Unknown":
		*l = LevelUnknown
	case "Ignore":
		*l = LevelIgnore
	default:
		*l = LevelUndefined
	}
	return nil
}

// String converts a Level into a string
func (l Level) String() string {
	switch l {
	case LevelCritical:
		return "Critical"
	case LevelHigh:
		return "High"
	case LevelMedium:
		return "Medium"
	case LevelLow:
		return "Low"
	case LevelExperimental:
		return "Experimental"
	case LevelUnknown:
		return "Unknown"
	case LevelIgnore:
		return "Ignore"
	}
	return ""
}

type Location struct {
	File      string `json:"file,omitempty"`       // File is the path relative to the search path.
	LineStart int    `json:"start_line,omitempty"` // LineStart is the first line of the affected code.
	LineEnd   int    `json:"end_line,omitempty"`   // LineEnd is the last line of the affected code.
	Class     string `json:"class,omitempty"`
	Method    string `json:"method,omitempty"`
}

type Link struct {
	Name string `json:"name,omitempty"` // Name of the link (optional)
	URL  string `json:"url"`            // URL of the document (mandatory)
}

// Level type used by severity and confidence values
type IdentifierType string

const (
	IdentifierTypeCVE = "cve"
	IdentifierTypeCWE = "cwe"
)

type Identifier struct {
	Type  IdentifierType `json:"type"`          // Type of the identifier (CVE, CWE, VENDOR_X, etc.)
	Name  string         `json:"name"`          // Name of the identifier for display purpose
	Value string         `json:"value"`         // Value of the identifier for matching purpose
	URL   string         `json:"url,omitempty"` // URL to identifier's documentation
}

// CVEIdentifier returns a structured Identifier for a given CVE-ID
// Given ID must follow this format: `CVE-YYYY-NNNNN`
func CVEIdentifier(ID string) Identifier {
	return Identifier{
		Type:  IdentifierTypeCVE,
		Name:  ID,
		Value: ID,
		URL:   fmt.Sprintf("https://cve.mitre.org/cgi-bin/cvename.cgi?name=%s", ID),
	}
}

// CWEIdentifier returns a structured Identifier for a given CWE ID
// Given ID must follow this format: `NNN` (just the number, no prefix)
func CWEIdentifier(ID int) Identifier {
	return Identifier{
		Type:  IdentifierTypeCWE,
		Name:  fmt.Sprintf("CWE-%d", ID),
		Value: strconv.Itoa(ID),
		URL:   fmt.Sprintf("https://cwe.mitre.org/data/definitions/%d.html", ID),
	}
}
