package issue

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
)

var testIssue = Issue{
	Tool:        "brakeman",
	Category:    CategorySast,
	Name:        "Possible command injection",
	Message:     "Possible command injection in application_controller",
	Description: "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
	CompareKey:  "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
	Location: Location{
		File:      "app/controllers/application_controller.rb",
		LineStart: 831,
		LineEnd:   832,
	},
	Severity:   LevelMedium,
	Confidence: LevelHigh,
	Solution:   "Use the system(command, parameters) method which passes command line parameters safely.",
	Identifiers: []Identifier{
		CVEIdentifier("CVE-2018-1234"),
	},
	Links: []Link{
		{
			Name: "Awesome-security blog post",
			URL:  "https://example.com/blog-post",
		},
		{
			URL: "https://example.com/another-blog-post",
		},
	},
}

var testIssueJSON = `{
  "tool": "brakeman",
  "category": "sast",
  "name": "Possible command injection",
  "message": "Possible command injection in application_controller",
  "description": "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
  "cve": "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
  "severity": "Medium",
  "confidence": "High",
  "solution": "Use the system(command, parameters) method which passes command line parameters safely.",
  "location": {
    "file": "app/controllers/application_controller.rb",
    "start_line": 831,
    "end_line": 832
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ],
  "links": [
    {
      "name": "Awesome-security blog post",
      "url": "https://example.com/blog-post"
    },
    {
      "url": "https://example.com/another-blog-post"
    }
  ],
  "priority": "Medium",
  "file": "app/controllers/application_controller.rb",
  "line": 831,
  "url": "https://example.com/blog-post"
}`

func TestMarshalJSON(t *testing.T) {
	b, err := json.Marshal(testIssue)
	if err != nil {
		t.Fatal(err)
	}

	var buf bytes.Buffer
	json.Indent(&buf, b, "", "  ")
	got := buf.String()

	if got != testIssueJSON {
		t.Errorf("Wrong JSON output. Expected:\n%s\nBut got:\n%s", testIssueJSON, got)
	}
}

func TestUnmarshalJSON(t *testing.T) {
	var got Issue
	if err := json.Unmarshal([]byte(testIssueJSON), &got); err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(got, testIssue) {
		t.Errorf("Wrong unmarshalled Issue. Expected:\n%#v\nBut got:\n%#v", testIssue, got)
	}
}

func TestMarshalJSON_WithEmptyIssue(t *testing.T) {
	issue := Issue{}

	// encode
	b, err := json.Marshal(issue)
	if err != nil {
		t.Fatal(err)
	}

	// indented buffer
	var buf bytes.Buffer
	json.Indent(&buf, b, "", "  ")
	got := buf.String()

	// expected JSON
	want := `{
  "tool": "",
  "category": "",
  "cve": "",
  "location": {}
}`

	// compare
	if got != want {
		t.Errorf("Wrong JSON output. Expected:\n%s\nBut got:\n%s", want, got)
	}
}
