# Analyzers Common Library

This repository contains Go packages you may use to create SAST analyzers.

## How to use the analyzers

Analyzers are shipped as Docker images.
All analyzers based on this library can be used the same way.
Here's an example using the
[find-sec-bugs](https://gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs) Docker image:

1. `cd` into the directory of the source code you want to scan
1. Run the Docker image:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume "$PWD":/tmp/app \
      --env CI_PROJECT_DIR=/tmp/app \
      registry.gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs:${VERSION} /analyzer run
    ```

1. The Docker container generates an artifact named `gl-sast-report.json` in the project directory.

`VERSION` can be replaced with the latest available release matching your GitLab version.

## Analyzers development

To update the analyzer:

1. Modify the Go source code.
1. Compile a Linux binary.
1. Build a new Docker image.
1. Run the analyzer against its test project.
1. Compare the generated report with what's expected.

Here's how to create a Docker image named `sast-analyzer` and test it:

```sh
GOOS=linux go build -o analyzer
docker build -t sast-analyzer .

docker run --rm \
  --volume "$PWD"/test/fixtures:/tmp/project \
  --env CI_PROJECT_DIR=/tmp/project \
  sast-analyzer \
  /analyzer run

diff test/fixtures/gl-sast-report.json test/expect/gl-sast-report.json
```

You can also compile the binary for your own environment and run it locally
but `analyze` and `run` probably won't work
since the runtime dependencies of the analyzer are missing.

Here's an illustration based on
[find-sec-bugs](https://gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs):

```sh
go build -o analyzer
./analyzer search test/fixtures
./analyzer convert test/fixtures/app/spotbugsXml.Xml > ./gl-sast-report.json
```

## How to use the library

You can easily bootstrap a new analyzer project by copying
the [template](template) to a new repository and implementing the TODOs.

Analyzer relies on the [`command`](command) Go package to implement
a command line that implements these sub-commands:

- `search` searches for a project that is supported by the analyzer.
- `analyze` performs the analysis in a given directory.
- `convert` converts the output to a `gl-sast-report.json` artifact.
- `run` performs all the previous steps consecutively.

All you need to do is to implement:
- a match function that implements [`command.MatchFunc`](command/search.go)
- a conversion function that implements [`command.ConvertFunc`](command/convert.go)
- an analyze function and a function that lists the flags for the `analyze` sub-command

See [template/main.go](template/main.go)

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
